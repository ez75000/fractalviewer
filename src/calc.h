// CALC HEADER
#include <complex.h>

// Definition of the mathematical "sheet"
typedef
struct Sheet	{complex center; // center of the sheet
				 long double hwidth, // half width
				             hheight, // half height
				             dx, dy; // mathematical 'pitch'
				 char** val; // color at each point
				}
Sheet;

// Initialize the mathematical model
int Initialize_Sheet(void);

// Frees memory etc
int Destroy_Sheet(Sheet sheet);

long double norm(complex z);

// Iteration function
#define MAXITER 50
complex f(complex u, complex c);

// Compute N(z, f), given a point z and an iteration function f.
char N(complex z, complex(*f)(complex, complex) );

