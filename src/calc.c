// CALC MODULE
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <complex.h>
#include "../etc/params.h"
#include "calc.h"

// Initialize the mathematical model
int Initialize_Sheet(void)
{
	Sheet sheet; // the mathematical model
	sheet.hwidth = (XMAX - XMIN)/2.;
	sheet.hheight = (YMAX - YMIN)/2.;
	sheet.center = (XMIN + sheet.hwidth, YMIN + sheet.hheight);
	sheet.dx = sheet.hwidth/(2.*WINWIDTH);
	sheet.dy = sheet.hheight/(2.*WINHEIGHT);
	sheet.val = (char**)malloc( WINHEIGHT*sizeof(char*) );
	for (int i = 0; i < WINHEIGHT; i++)
		sheet.val[i] = (char*)malloc( WINWIDTH );
	return 0;
}

// Frees memory etc
int Destroy_Sheet(Sheet sheet)
{
	for (int i = 0; i < WINHEIGHT; i++)
		free( sheet.val[i] );
	free( sheet.val );
}

// |z|^2
long double norm(complex z)
{
	return creal(z)*creal(z) + cimag(z)*cimag(z);
}

// Iteration function
complex f(complex u, complex c)
{
	return u*u + c; // for Mandelbrot set
}

// Compute N(z, f), given a point z and an iteration function f.
char N(complex Z, complex(*f)(complex, complex) )
{
	char N = '0';
	complex u = 0.;
	do
	{
		u = (*f)(u, Z);
		N = N + 1;
	}
	while( norm(u) < 4. );
	return N;
}
