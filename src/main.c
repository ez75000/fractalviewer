// FRACTAL VIEWER, MAIN.
#include <stdio.h>
#include <SDL2/SDL.h>
#include "../etc/params.h"
#include "calc.h"

int main(int argc, char* argv[])
{
	//////////////////////////////////////////////////////////////////////
	////// SHEET AND VIEWER INITIALIZATION ///////////////////////////////
	//////////////////////////////////////////////////////////////////////
	// [Sheet initialization]
	if ( Initialize_Sheet() )
	{
		printf("error: Sheet creation." );
		exit(EXIT_FAILURE);
	}
	// [SDL Initialization:]
	SDL_Window* win;
	SDL_Event ev;
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("error: SDL initialization: %s", SDL_GetError() );
		SDL_Quit();
		exit(EXIT_FAILURE);
	}
	// [window creation:]
	win = SDL_CreateWindow("Fractal Viewer",
							SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
							WINWIDTH, WINHEIGHT,
							SDL_WINDOW_SHOWN);
	if (win == NULL)
	{
		printf("error: Window creation: %s", SDL_GetError() );
		SDL_Quit();
		exit(EXIT_FAILURE);
	}
	//////////////////////////////////////////////////////////////////////
	////// EVENTS LOOP ///////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////
	while( 1 )
	{
		SDL_WaitEvent(&ev);
		if (ev.window.event == SDL_WINDOWEVENT_CLOSE) break;
	}
	//////////////////////////////////////////////////////////////////////
	////// ENDING ////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////
	// [SDL cleanup:]
	SDL_Delay(500);
	SDL_DestroyWindow(win);
	SDL_Quit();
	return(EXIT_SUCCESS);
}


