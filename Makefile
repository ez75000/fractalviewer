
EXEC    = fv
CC      = gcc
CFLAGS  =
LIBS    = -lSDL2

SRCDIR = src
BINDIR = bin
ETCDIR = etc
DOCDIR = doc

OBJS = $(BINDIR)/main.o $(BINDIR)/calc.o

all: $(OBJS)
	$(CC) $(CFLAGS) -o$(BINDIR)/$(EXEC).ex $^ $(LIBS)

tidy:
	cd $(BINDIR) && rm -f *.o
	cd $(ETCDIR) && rm -f *.o *.gch
	cd $(SRCDIR) && rm -f *.o *.gch

run:
	./$(BINDIR)/$(EXEC).ex

clean:
	make tidy
	cd $(BINDIR) && rm -f *.ex
	
$(BINDIR)/main.o:  $(SRCDIR)/main.c  \
                   $(SRCDIR)/calc.h  \
                   $(ETCDIR)/params.h
	$(CC) $(CFLAGS) -c $^
	mv main.o $(BINDIR)/

$(BINDIR)/calc.o:  $(SRCDIR)/calc.c  \
                   $(SRCDIR)/calc.h  \
                   $(ETCDIR)/params.h
	$(CC) $(CFLAGS) -c $^
	mv calc.o $(BINDIR)/
